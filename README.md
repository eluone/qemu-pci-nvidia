# Challenge

- Fedora 35 Host to provide NVIDIA GPU Passthrough to QEMU Win 10 Guest
- "Full" speed VM to be able to support gaming in Windows guest
- This setup uses two monitors and two graphics cards

## Host Machine

- AMD Ryzen 5 2600 @ 3.4GHz
- MSI MPG X570 Gaming Plus Motherboard
- 32 Gb RAM
- NVIDIA GeForce GTX 1660Ti (Guest)
- AMD ATI Radeon HD 5000/6000/7350/8350 Series GFX (Host)
- ADATA XPG SX8200 Pro 512GB M.2 (PCIe Gen3x4) (Host Storage)
- 250GB Virtual HDD (Guest Storage) 

## Software

- KDE spin of Fedora - [Fedora Project](https://spins.fedoraproject.org/kde/download/index.html)
- VirtIO Windows Drivers - [VirtIO Git](https://github.com/virtio-win/virtio-win-pkg-scripts/blob/master/README.md)
- A Windows 10 install ISO - [Microsoft](https://www.microsoft.com/en-gb/software-download/windows10iso)

## Host Configuration

From fresh Fedora 35 install

### BIOS Configuration

- Enable Intel / AMD virtualization in BIOS

### Additional Packages

- edk2-ovmf
- virt-manager

### Installation of extra required packages:

```
sudo dnf install edk2-ovmf virt-manager
```

### Update GRUB command line

These are the initial settings to isolate our NVIDIA GFX card.  
- We have used 1x AMD and 1x NVIDIA GFX cards
- The open source NVIDIA drivers are going to be disabled so our host doesn't use the card.
- Edit GRUB_CMDLINE_LINUX to include amd_iommu=on

```
sudo nano /etc/default/grub
```

```
GRUB_CMDLINE_LINUX="module_blacklist=nouveau quiet amd_iommu=on"
```

Apply GRUB changes:

```
sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
```

Reboot to use changes.

### Check NVIDIA GFX configuration

- Make a note of hardware IDs for NVIDIA GPU and Audio controllers.
- Make a note of hardware IDs for additional USB/Serial controllers on NVIDIA card to stub later.


```
lspci -nn | grep -i NVIDIA
```

Our NVIDIA card is identified as:

```
23:00.0 VGA compatible controller [0300]: NVIDIA Corporation TU116 [GeForce GTX 1660 Ti] [10de:2182] (rev a1)
23:00.1 Audio device [0403]: NVIDIA Corporation TU116 High Definition Audio Controller [10de:1aeb] (rev a1)
23:00.2 USB controller [0c03]: NVIDIA Corporation TU116 USB 3.1 Host Controller [10de:1aec] (rev a1)
23:00.3 Serial bus controller [0c80]: NVIDIA Corporation TU116 USB Type-C UCSI Controller [10de:1aed] (rev a1)
```

### Check GPU and associated devices are in same iommu group

We have to pass an entire IOMMU group to the VM.

Check which devices are in each IOMMU group (Using a sort on devices)

```
find /sys/kernel/iommu_groups/ -type l | sort -t / -k 5 -n
```

xxx EXAMPLE OUTPUT HERE xxx
```
/sys/kernel/iommu_groups/21/devices/0000:23:00.0
/sys/kernel/iommu_groups/21/devices/0000:23:00.1
/sys/kernel/iommu_groups/21/devices/0000:23:00.2
/sys/kernel/iommu_groups/21/devices/0000:23:00.3
```
### Use vfio-pci and pci-stub drivers for NVIDIA GFX card

Isolate NVIDIA GFX card and stub USB/Serial (onboard NVIDIA card) by inputting your relevant hardware IDs on GRUB_CMDLINE_LINUX

From our NVIDIA Card above: 
- GeForce GTX 1660 Ti VGA controller [10de:2182]
- Audio Controller [10de:1aeb]
- USB 3.1 Host Controller [10de:1aec]
- USB Type-C UCSI Controller [10de:1aed]

```
sudo nano /etc/default/grub
```

edit GRUB_CMDLINE_LINUX to include rd.driver.pre=vfio-pci vfio-pci.ids=10de:2182,10de:1aeb pci-stub.ids=10de:1aec,10de:1aed

```
GRUB_CMDLINE_LINUX="module_blacklist=nouveau quiet amd_iommu=on rd.driver.pre=vfio-pci vfio-pci.ids=10de:2182,10de:1aeb pci-stub.ids=10de:1aec,10de:1aed"
```

Apply GRUB changes:

```
sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
```

Reboot to use changes.

### Check kernel driver in use for isolated hardware IDs

```
lspci -k | grep -i NVIDIA -A 3
```

Our NVIDIA card is shown as using the vfio-pci and pci-stub drivers ready for passthrough:

```
23:00.0 VGA compatible controller: NVIDIA Corporation TU116 [GeForce GTX 1660 Ti] (rev a1)
        Subsystem: eVga.com. Corp. Device 1261
        Kernel driver in use: vfio-pci
        Kernel modules: nouveau, nvidia_drm, nvidia
23:00.1 Audio device: NVIDIA Corporation TU116 High Definition Audio Controller (rev a1)
        Subsystem: eVga.com. Corp. Device 1261
        Kernel driver in use: vfio-pci
        Kernel modules: snd_hda_intel
23:00.2 USB controller: NVIDIA Corporation TU116 USB 3.1 Host Controller (rev a1)
        Subsystem: eVga.com. Corp. Device 1261
        Kernel driver in use: pci-stub
23:00.3 Serial bus controller: NVIDIA Corporation TU116 USB Type-C UCSI Controller (rev a1)
        Subsystem: eVga.com. Corp. Device 1261
        Kernel driver in use: pci-stub
        Kernel modules: i2c_nvidia_gpu
```

## Guest VM Configuration

### Initial Configuration

Using virtmanger guided setup to start our Win VM:
- Local install media (Select Win Install ISO)
- 16GB RAM
- 2 vCPUs
- Enable storage for VM (40Gb Default)
- Name VM
- Select "Customise configuration before install"

### Custom Configuration

We are going to select a UEFI firmware image and use virtIO for storage and network.
To load the virtIO drivers during the Windows installation we are going to add a second virtual CD drive for the ISO.

|Section|Options|
|:---|:---|
|Overview|Firmware: UEFI x86_64 OVMF_CODE.fd|
|CPUs|Copy host CPU configuration|
|SATA Disk 1|Disk bus: VirtIO|
|NIC:xx:xx:xx|Device Model: virtio|
|Add Hardware|Storage > "Select or create custom storage" "Manage" > Select virtIO ISO > Device Type: "CDROM device"|

- Follow Win10 installer as normal
- At "Where do you want to install Windows?" drive selection page you will have to use the "Load driver" button (OK at dialog to auto search)
- Select the VirtIO drivers from the iso for Win10 to see the VM HDD (Red Hat VirtIO SCSI controller ...amd64\w10\viostor.inf)
- Drive 0 should show as usable now
- Continue with install
- Shutdown VM

### Add NVIDIA Card to VM

- Install NVIDIA geforce experience
- Download latest NVIDIA drivers


## Errors

```
Error creating snapshot: Operation not supported: internal snapshots of a VM with pflash based firmware are not supported
```

- As we are using a UEFI based firmware for the VM we can't use snapshots. :(

# Future Options

- Passthrough dedicated NIC to VM?
